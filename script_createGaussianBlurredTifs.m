% script to run thickness and compression estimation for differently
% gaussian blurred versions of the same volume

% 

% inputStackFileName = '/home/thanuja/projects/data/FIBSEM_dataset/largercubes/s502/s502.tif';
%inputStackFileName = '/home/thanuja/projects/data/FIBSEM_dataset/XYshiftedStacks/s502/xShifted/s502xShiftedGap_xShiftedStack_sliceID101.tif';
inputStackFileName = '/home/thanuja/projects/data/rita/cropped_hist_aaa/D4/D4.tif';

% outputRoot = '/home/thanuja/projects/data/FIBSEM_dataset/gaussianBlurred/s502_xShifted_gap2_slice101';
outputRoot = '/home/thanuja/projects/data/rita/gauss/D4_aaa_hist';

% sigmaG = [0.2 0.5 1 1.5 2 3 4 5 7 8 10];
sigmaG = [0.5];
maskG = 5;
startInd = 1;
endInd = 500;

imgFileName = strsplit(inputStackFileName,'/');
imgFileName = char(imgFileName(end));

for i = 1: length(sigmaG)
    subDirStr = sprintf('sig%s',num2str(sigmaG(i)));
    checkAndCreateSubDir(outputRoot,subDirStr);
    outputFileName = fullfile(outputRoot,subDirStr,imgFileName);   
    gaussianFilterOnTiffStack(inputStackFileName,outputFileName,...
    sigmaG(i),maskG,startInd,endInd);
    disp(outputFileName);
end