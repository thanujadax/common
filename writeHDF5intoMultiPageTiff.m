function writeHDF5intoMultiPageTiff()

outputFileName = '/home/thanuja/RESULTS/isbi2012/CNN/train/probMaps20161107_20_30.tif';
% h5FileName_membranes = '/home/thanuja/projects/classifiers/greentea/caffe_neural_models/isbi12_s2/isbi_20161018.h5';
h5FileName_membranes = '/home/thanuja/projects/classifiers/greentea/caffe_neural_models/isbi12_s5_20im/isbi_20161107-train20_30.h5';
dataSet = '/main';
membraneData = h5read(h5FileName_membranes,dataSet);

% todo: make sure not to make all values {0,1} instead of [0,1]

% 2D: 512 x 512 x 2 x 30
membraneData = shiftdim(membraneData,3);
membraneProbMaps = (membraneData(:,:,:,1));
membraneProbMaps = shiftdim(membraneProbMaps,1);

writeImageMatrixToTiffStack(invertImage(membraneProbMaps),outputFileName);
% writeImageMatrixToTiffStack(uint32(~membraneProbMaps),outputFileName)