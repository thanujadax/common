function convertPNGimages2TiffStack(inputDir,outputFileName)
% inputDir should contain black and white (0,1) images
inputFileList = dir(fullfile(inputDir,'*.png'));
im1 = imread(fullfile(inputDir,inputFileList(1).name));
[sizeR,sizeC] = size(im1);
% imMat = single(zeros(sizeR,sizeC,length(inputFileList)));
imMat = uint32(zeros(sizeR,sizeC,length(inputFileList)));

for i=1:length(inputFileList)
    % im = single(imread(fullfile(inputDir,inputFileList(i).name)));
    im = uint32(imread(fullfile(inputDir,inputFileList(i).name)));
    % imMat(:,:,i) = im./255;
    imMat(:,:,i) = im;
end

writeImageMatrixToTiffStack(imMat,outputFileName);