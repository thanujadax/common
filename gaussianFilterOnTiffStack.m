function gaussianFilterOnTiffStack(inputStackFileName,outputFileName,...
    sigmaG,maskG,startInd,endInd)

% inputStackFile -  
% outputPath - 
% sigmaG - gaussian blur
% maskG - square mask size to apply gaussian blur [maskG maskG]
% startInd,endInd - to pick a custom range. keep empty to use the entire
% stack

% inputStackFileName = '';
% outputFileName = '';
% sigmaG = '';
% maskG = '';
% startInd = 101;
% endInd = 300;

% read tif stack into array
inputImageStack = uint8(readTiffStackToArray(inputStackFileName));
[sizeR,sizeC,sizeZ] = size(inputImageStack);

if(endInd>sizeZ)
    disp('endInd > sizeZ. setting endInd = sizeZ')
    endInd = sizeZ;
end

if(startInd>endInd)
    disp('startInd > endInd or sizeZ. setting startInd = 1')
    startInd = 1;
end
numSections = numel(startInd:endInd);
outputStack = uint8(zeros(sizeR,sizeC,numSections));

% apply gaussian blur
k = 0;
for i=startInd:endInd
    im_in = inputImageStack(:,:,i);
    im_out = gaussianFilter(im_in,sigmaG,maskG);
    k = k + 1;
    outputStack(:,:,k) = im_out;
end
% write modified array into tif stack
%imwrite(outputStack,outputFileName,'TIFF');
writeImageMatrixToTiffStack(outputStack,outputFileName);