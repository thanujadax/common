function [err,sem] = meanabserror(a,b)

err = mean(abs(a-b));

sem = std(abs(a-b))/sqrt(numel(a));