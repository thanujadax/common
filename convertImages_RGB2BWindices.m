function convertImages_RGB2BWindices(inputDir,outputDir,inputFmt)
% convert all color images in inputDir to BW. 
% Black pixels stay black. All colors should be replaced with white

inputDir = '/home/thanuja/RESULTS/isbi2012/rfc_ilp_cnn/png';
outputDir = '/home/thanuja/RESULTS/isbi2012/rfc_ilp_cnn/bwidx';


% read list of png files to convert to bw
inputFmt = 'png';

inputFileList = dir(fullfile(inputDir,strcat('*.',inputFmt)));
for i=1:length(inputFileList)
    imIn = double(imread(fullfile(inputDir,inputFileList(i).name),inputFmt));
    % [imBW,cmap] = rgb2ind(imIn,maxColors);
    % imBW = rgb2gray(imIn);
    indIm = convertRGBtoIndexImage(imIn);
    indIm = indIm./(max(max(indIm)));
    outputFileName = fullfile(outputDir,inputFileList(i).name);
    imwrite(indIm,outputFileName);
end