function readResizeWriteTff()
inputFileName = '/home/data/DATA/ETH/ssSEM_AFM_thickness/01/dataset_01.tif';
outputFileName ='/home/data/DATA/ETH/ssSEM_AFM_thickness/small/1500.tif';

inputImageStack = readTiffStackToArray(inputFileName);

r_start = 1;
c_start = 1;

r_end = 1500;
c_end = 1500;

writeImageMatrixToTiffStack(inputImageStack(r_start:r_end, c_start:c_end,:),...
    outputFileName);