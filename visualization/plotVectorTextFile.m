fileID = fopen('compressionMeans.txt','r');
formatSpec = '%f';
A = fscanf(fileID,formatSpec);

plot(A(20:500))
xlabel('Image index','FontSize',30)
ylabel('Stretching','FontSize',30)
set(gca,'FontSize',30,'LineWidth',1.5)
