function watershedColoredEdges = overlayWatershedOnImage(originalImg,...
        watershedImg)
% overlay watershed edges on to original image


% watershedEdgePixels = (watershedImg==0);
% watershedColoredEdges = cat(3,watershedEdgePixels,...
%     zeros(size(watershedImg)),zeros(size(watershedImg))); % red
% 
% imshow(originalImg);
% hold on
% h = imshow(watershedColoredEdges);
% set(h, 'AlphaData', 0.5);
% hold off

watershedEdgePixels = (watershedImg==0);
watershedColoredEdges = cat(3,watershedEdgePixels,...
    zeros(size(watershedImg)),zeros(size(watershedImg))); % red
% figure;imshow(originalImg)
% figure;imshow(watershedColoredEdges);
% figure;imshowpair(originalImg,watershedColoredEdges)
% segmentationOut = removeThickBorder(segmentationOut,marginSize);
figure(101);imshow(watershedColoredEdges)
set(0,'CurrentFigure',101)
hold on
h = imshow(originalImg);
hold off
alpha = ones(size(originalImg)) .* 0.5;
set(h, 'AlphaData', alpha);