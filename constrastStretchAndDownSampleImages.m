% constrast stretched and downsampled images

inputDir = '/home/thanuja/projects/data/myelin/ssSEM/myelinTiles/raw/s1503';
outputRoot = '/home/thanuja/projects/data/myelin/ssSEM/myelinTiles/downsampled_raw';
dirName = 's1503';
checkAndCreateSubDir(outputRoot,dirName);
outputDir = fullfile(outputRoot,dirName);

contrastStretch = 1;
downSampleScale = 0.5;

ext = 'tif';

% first try only constrast stretching
allInputFilesDir = dir(fullfile(inputDir,strcat('*',ext)));
numInputFiles = length(allInputFilesDir);

for i = 1:numInputFiles
    inputIm = double(imread(fullfile(inputDir,allInputFilesDir(i).name)));
    if(contrastStretch)
        % outputIm = imadjust(inputIm,stretchlim(inputIm),[]);
        outputIm = normalizeToZeroOne(inputIm);
    else
        outputIm = inputIm;
    end
    % rescale
    
    % wrtie output
    outputFileName = fullfile(outputDir,allInputFilesDir(i).name);
    imwrite(outputIm,outputFileName);
    
end