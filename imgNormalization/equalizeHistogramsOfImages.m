function equalizeHistogramsOfImages(inputDir,outputDir,referenceInd)

% Inputs
% referenceInd - imageID for the image whose equalized histogram should be
% applied to the other images

inputDir = '/home/thanuja/projects/data/rita/sstem_tiles_without_fold/Ee1b';
outputDir = '/home/thanuja/projects/data/rita/normalizedTiles/hist/Ee1b';
referenceInd = 1;

fileExt = 'tif'; % the only other acceptable value is 'tiff' ;)

if(isempty(fileExt))
    fileExt = 'tif';
end

ext2 = strcat('*.',fileExt);

inputDirFileNames = fullfile(inputDir,ext2);
fileList = dir(inputDirFileNames);

numTifTiles = length(fileList);
if(numTifTiles<=0)
    str1 = sprintf('no tif files found in directory %s',inputDir);
    error(str1)
end

% first read reference image
inputTifFileName = fullfile(inputDir,fileList(1).name);
inputImage = double(readTiffStackToArray(inputTifFileName));
% apply zero to one normalization
inputIm_normalized = normalizeToZeroOne(inputImage);
% do histogram equalization
im_histEqualized = histeq(inputIm_normalized);
% get histogram
[counts,binLocations] = imhist(im_histEqualized);
% apply histogram to all other images

for i=1:numTifTiles
    inputTifFileName = fullfile(inputDir,fileList(i).name);
    outputTifFileName = fullfile(outputDir,fileList(i).name);
    imIn = readTiffStackToArray(inputTifFileName);
    imIn_zeroOne = normalizeToZeroOne(imIn);
    imOut = histeq(imIn_zeroOne,counts);
    imwrite(imOut,outputTifFileName);
end