function imOut = normalizeToZeroOne(imIn)

imOut = imIn - min(min(imIn));

imOut = imOut ./ (max(max(imOut)));