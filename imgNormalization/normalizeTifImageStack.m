function normalizeTifImageStack(inputTifFileName,outputTifFileName)
% read tiff stack
inputImageStack = readTiffStackToArray(inputTifFileName);

% normalize by making mean = 0 and SD = 1
[sizeR,sizeC,sizeZ] = size(inputImageStack);
normalizedStack

% uses matlab functions from 'common' directory

% read image stack into array
% for each image (independently of others), center pixel intensities around
% zero

