function normalizeAllTifTilesInDirectory(inputDir,outputDir,fileExt)

inputDir = '/home/thanuja/projects/data/rita/sstem_tiles_without_fold/E1';
outputDir = '/home/thanuja/projects/data/rita/normalizedTiles/basic/E1';

fileExt = 'tif'; % the only other acceptable value is 'tiff' ;)

if(isempty(fileExt))
    fileExt = 'tif';
end

ext2 = strcat('*.',fileExt);

inputDirFileNames = fullfile(inputDir,ext2);
fileList = dir(inputDirFileNames);

numTifTiles = length(fileList);
if(numTifTiles<=0)
    str1 = sprintf('no tif files found in directory %s',inputDir);
    error(str1)
end

for i=1:numTifTiles
    inputTifFileName = fullfile(inputDir,fileList(i).name);
    outputTifFileName = fullfile(outputDir,fileList(i).name);
    normalizeTifTile(inputTifFileName,outputTifFileName);
end