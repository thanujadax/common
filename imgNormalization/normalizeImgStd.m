function imOut = normalizeImgStd(imIn)

% imIn - image to be normalized. 8 bit grayscale image

% imOut - normalized image calculated as follows
% basic image normalization
% imOut = imIn - mean(imIn)
%         _________________
%         sqrt(var(imIn))

imIn = double(imIn);
[sizeR,sizeC] = size(imIn);
numPix = sizeR * sizeC;

imVect = double(reshape(imIn,numPix,1)); % column vector containing image pixels

meanI = mean(imVect);
stdI = std(imVect);

imOut = double((imIn - meanI) ./ stdI);



