function normalizeTifTile(inputTifFileName,outputTifFileName)
% reads single page tif image. writes the normalized version
% uses matlab functions from 'common' directory
% read tiff stack
inputImage = readTiffStackToArray(inputTifFileName);

% normalize by making mean = 0 and SD = 1
a = normalizeImgStd(inputImage);

% normalize to zero-one range
normalizedImg = normalizeToZeroOne(a);
% b = normalizeToZeroOne(inputImage);

% d = (b - normalizedImg).^2;
% err = sum(sum(d))

% writeImageMatrixToTiffStack(normalizedImg,outputTifFileName);
imwrite(normalizedImg,outputTifFileName);