function rgbImage = indexImg2rgbImg(indexImg)

uniqueInds = unique(indexImg);
numIDs = length(uniqueInds);
[sizeR,sizeC] = size(indexImg);
rMat = zeros(sizeR,sizeC);
gMat = zeros(sizeR,sizeC);
bMat = zeros(sizeR,sizeC);

rgbImage = zeros(sizeR,sizeC,3);

for i=1:numIDs
    % pick random color (RGB vals)
    R = rand(1); G = rand(1); B = rand(1);
    
    % get regions for this cell and the internal pixels. set RGB
    ind_i = uniqueInds(i);
    regionPixels = (indexImg==ind_i);
    rMat(regionPixels) = R;
    gMat(regionPixels) = G;
    bMat(regionPixels) = B;
    
    
    rgbImage(:,:,1) = rMat;
    rgbImage(:,:,2) = gMat;
    rgbImage(:,:,3) = bMat; 
        
end