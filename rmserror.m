function err = rmserror(a,b)

err = sqrt(mean((a - b).^2));