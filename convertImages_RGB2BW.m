function convertImages_RGB2BW(inputDir,outputDir)
% convert all color images in inputDir to BW. 
% Black pixels stay black. All colors should be replaced with white

% inputDir = '/home/thanuja/RESULTS/isbi2012/001/png';
% outputDir = '/home/thanuja/RESULTS/isbi2012/001/bw';

threshold = 0; % any pixel with value > 0 will be set to 1

% read list of png files to convert to bw
inputFileList = dir(fullfile(inputDir,'*.png'));

for i=1:length(inputFileList)
    imIn = double(imread(fullfile(inputDir,inputFileList(i).name)));
    imBW = im2bw(imIn,threshold);
    % imBW = rgb2gray(imIn);
    outputFileName = fullfile(outputDir,inputFileList(i).name);
    imwrite(imBW,outputFileName);
end