function writeImageMatrixToTiffStack(imgdata,outputFileName)
% imwrite doesn't work for large images
% this works for arbitrarily large stacks :-)
% remove outputfile if already exists
if exist(outputFileName,'file') == 2
    delete(outputFileName);
end



for i=1:size(imgdata,3)
    t = Tiff(outputFileName,'a');
tagstruct.ImageLength = size(imgdata,1);
tagstruct.ImageWidth = size(imgdata,2);
tagstruct.Photometric = Tiff.Photometric.MinIsBlack;
tagstruct.BitsPerSample = 64; % for 32 bit images
tagstruct.SamplesPerPixel = 1;
tagstruct.RowsPerStrip = 16;
tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
tagstruct.SampleFormat = Tiff.SampleFormat.IEEEFP; % UInt, Int, IEEEFP
tagstruct.Software = 'MATLAB';

t.setTag('ExtraSamples', Tiff.ExtraSamples.AssociatedAlpha);

t.setTag(tagstruct);
    t.write(imgdata(:,:,i));    
    t.close();
end
    
% for K=1:length(imgdata(1, 1, :))
%     imwrite(imgdata(:, :, K), outputFileName, 'WriteMode', 'append',  'Compression','none');
% 
% end

