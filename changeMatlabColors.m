% Don't use system color
com.mathworks.services.Prefs.setBooleanPref('ColorsUseSystem',0);
 
% Use specified colors for foreground/background (instead of the default black on white)
com.mathworks.services.Prefs.setColorPref('ColorsBackground',java.awt.Color.black);
com.mathworks.services.ColorPrefs.notifyColorListeners('ColorsBackground');

com.mathworks.services.Prefs.setColorPref('ColorsText',java.awt.Color.green);
com.mathworks.services.ColorPrefs.notifyColorListeners('ColorsText');