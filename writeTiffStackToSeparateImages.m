function writeTiffStackToSeparateImages(inputImageVolumeFileName,saveFilePath,fileType,numImg)

inputImageStack = readTiffStackToArray(inputImageVolumeFileName);

if(isempty(numImg))
    numImg = size(inputImageStack,3);
end
if(isempty(fileType))
 fileType = 'tif';
end

for i=1:numImg
    imageName = sprintf('%03d.%s',(i-1),fileType);
    outputFullFile = fullfile(saveFilePath,imageName);
    disp(outputFullFile);
    image_i = inputImageStack(:,:,i);
    image_i = image_i./255;
    imagesc(image_i);
    imwrite(image_i,outputFullFile);
end