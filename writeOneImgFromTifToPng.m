% inputImageVolumeFileName = '/home/thanuja/projects/data/FIBSEM_dataset/XYshiftedStacks/s502/xShifted/s502xShiftedGap01_xShiftedStack_sliceID101.tif';
% saveFilePath = '/home/thanuja/Dropbox/ETH/PUBLICATIONS/sectionThickness/thicknessISBI2016/images';
% fileType = 'png';

% To write the first (or up to N images into separate png file(s)
%numImg = 1;
%writeTiffStackToSeparateImages(inputImageVolumeFileName,saveFilePath,fileType,numImg);


% to write an image given by in imgIndex
inputImageVolumeFileName='/home/thanuja/projects/data/FIBSEM_dataset/largercubes/s502/s502.tif';
saveFilePath = '/home/thanuja/projects/RESULTS/compression/s502_peak_valley';
fileType = 'png';
imgIndex = 294;
writeOneImgFromTifStackToPNG(inputImageVolumeFileName,saveFilePath,fileType,imgIndex);