function indexIm = convertRGBtoIndexImage(imIn)

% binarize image
if(size(imIn,3)==3)
    imIn = rgb2gray(imIn);
end
imIn = im2bw(imIn,0);

% get connected components
cc = bwconncomp(imIn);
% assign unique IDs for each CC
indexIm = zeros(size(imIn));

for i=1:length(cc.PixelIdxList)
    indexIm(cc.PixelIdxList{i}) = i;
end
