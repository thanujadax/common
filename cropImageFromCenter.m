function imOut = cropImageFromCenter(imIn,q1,q2)
% q1 - sizeR of cropped image
% q2 - sizeC of cropped image
q1 = floor(q1);
q2 = floor(q2);
[p3, p4,~] = size(imIn);
% q1 = 50; // size of the crop box
i3_start = ceil((p3-q1)/2); % or round instead of floor; using neither gives warning
if(i3_start==0)
    i3_start = 1;
end
i3_stop = i3_start + q1 -1;

i4_start = ceil((p4-q2)/2);
if(i4_start==0)
    i4_start = 1;
end
i4_stop = i4_start + q2 -1;

imOut = imIn(i3_start:i3_stop, i4_start:i4_stop, :);