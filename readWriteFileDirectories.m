% Read files from directory and write processed files to another directory

%% file paths and names

% filename = '/home/thanuja/projects/drosophila-l3/stack2/raw/00.tif';
% filename = '/home/thanuja/projects/drosophila-l3/stack2/classification/schmidhuber/median_filtered/neurons/neurons0000.png';
% filename = '/home/thanuja/projects/drosophila-l3/stack2/classification/schmidhuber/median_filtered/membrane/00_schmidhuber_membrane.tiff';
% filename = '/home/thanuja/projects/drosophila-l3/stack2/groundtruth/result_0000.tiff';

% writeFilePath = '/home/thanuja/projects/toyData/set9/groundtruth';
% writeFilePath = '/home/thanuja/projects/toyData/set9/membranes';
% writeFilePath = '/home/thanuja/projects/toyData/set9/neurons';
% writeFilePath = '/home/thanuja/projects/toyData/set9/raw';

% inputDirectoryName = '/home/thanuja/projects/drosophila-l3/stack2/raw/';
% inputDirectoryName =  '/home/thanuja/projects/drosophila-l3/stack2/classification/schmidhuber/median_filtered/neurons/';
% inputDirectoryName = '/home/thanuja/projects/drosophila-l3/stack2/classification/schmidhuber/median_filtered/membrane/';
% inputDirectoryName = '/home/thanuja/projects/drosophila-l3/stack2/groundtruth/';

inputDirectoryName = '/home/thanuja/DATA/ssSEM/20161215/contrastAdjusted';

imgFileExtension = 'tif';

% writeFilePath = '/home/thanuja/projects/inputData/trainingHalf/raw/';
% writeFilePath = '/home/thanuja/projects/inputData/trainingHalf/neurons/';
% writeFilePath = '/home/thanuja/projects/inputData/trainingHalf/membranes/';

% writeFilePath = '/home/thanuja/projects/inputData/trainingHalf';
% writeFilePath = '/home/thanuja/projects/inputData/testingHalf';
writeFilePath = '/home/thanuja/DATA/ssSEM/20161215/tiff_small';

% writeSubDir = 'raw';
% writeSubDir = 'neurons';
% writeSubDir = 'membranes';
% writeSubDir = 'groundtruth';
writeSubDir = '';
writeType = 'tif';

%% param
dimx = 4500;
dimy = 4500;

startRow = 3000;
stopRow = startRow -1 + dimy;

startCol = 3000;
stopCol = startCol - 1 + dimx;

numDim = 1;
%% read images from input directory

imageFileDirectoryPath = fullfile(inputDirectoryName,strcat('*',imgFileExtension));
str1 = sprintf('Search path: %s',imageFileDirectoryPath);
disp(str1)
imageFiles = dir(imageFileDirectoryPath);
nfiles = length(imageFiles);    % Number of files found
str1 = sprintf('Number of files found: %d',nfiles);
disp(str1)
%% write to output directory
% A = double(imread(filename));
% A = A./(max(max(A)));
% A = A./255;

k = 00; % file index
for i=1:nfiles
    currentInputFileName = imageFiles(i).name;
    inputFullFile = fullfile(inputDirectoryName,currentInputFileName);
    disp('Input file: ');
    disp(inputFullFile);
    A = double(imread(inputFullFile));
    A = A./255;
    B = (A(startRow:stopRow,startCol:stopCol,:));
% B = A;
    writeName = sprintf('%02d.%s',k,writeType);
    writeFileName = fullfile(writeFilePath,writeSubDir,writeName);
    disp('Output file: ')
    disp(writeFileName)
    imwrite(B,writeFileName,writeType)
    figure;imshow(B);
    k = k + 1;
end
        