function saveIntermediateImage(ImgIn,rawImageID,intermediateImgDescription,...
    saveIntermediateImagesPath,saveOutputFormat)

outputFileName = sprintf('%s_%s.%s',rawImageID,intermediateImgDescription,saveOutputFormat);
outputFullFile = fullfile(saveIntermediateImagesPath,outputFileName);
imwrite(ImgIn,outputFullFile,saveOutputFormat)