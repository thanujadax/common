function writeOneImgFromTifStackToPNG(inputImageVolumeFileName,saveFilePath,fileType,imgIndex)

% from a tiff stack of many images, write the image with imgIndex to PNG

inputImageStack = readTiffStackToArray(inputImageVolumeFileName);

% numImg = size(inputImageStack,3);

%fileType = 'tif';


    imageName = sprintf('%d.%s',imgIndex,fileType);
    outputFullFile = fullfile(saveFilePath,imageName);
    disp(outputFullFile);
    image_i = inputImageStack(:,:,imgIndex);
    image_i = image_i./255;
    imagesc(image_i);
    imwrite(image_i,outputFullFile);
